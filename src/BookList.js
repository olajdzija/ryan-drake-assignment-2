let linkClick=0;
function bookChosen(event){
    console.log("book Chosen Activate ");   
if(linkClick==0){
  //gray for some reason does not show well on my screen
  event.target.parentNode.style.color = "orange";
  linkClick=1;
}
else if(linkClick==1){
  event.target.parentNode.style.color = "black";
  linkClick=0;
}
 }

 function clickAll(){
  var chkBx = document.getElementsByClassName('bookLink');
  var classes = Array.prototype.slice.call(chkBx).map(function(element) {
    return element.value;
    });
  console.log("book clickAll Activate "+ classes);   
 for(var i = 0; i < chkBx.length; i++){
  console.log(" clickAll loop # "+ i); 
  console.log(" clickAll loop item "+ chkBx[i]); 
  chkBx[i].parentNode.click();
 }
 // classes.forEach(function(a) {
 // a.click();
 // });
 }


function BookList(props) {
  return (
    <ul>
            <button  onClick={clickAll}>select all books</button>


    {
        props.books.map((book, index) =>
          <li className="bookLink" onChange={bookChosen}>
            <input    type="checkbox" checked={book.deleteFlag} onClick={() => props.delete(index)} />{book.title}
          </li>
        )
    }

    </ul>
  );
}

export default BookList;
