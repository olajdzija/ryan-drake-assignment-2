import { useState } from 'react';
import './App.css';
import './style.css';
import List from './BookList';
import { getSuggestedQuery } from '@testing-library/react';

function App() {

  let [books, setBooks] = useState([
    {deleteFlag: false, title:"The Bible"},
    {deleteFlag: false, title:"Where the Red Fern Grows"},
    {deleteFlag: false, title:"Robinson Crusoe"}
  ]);

  let [newTitle, setNewTitle] = useState('');

 

  function addBook() {
    if(!newTitle){ 
      //failing to add a book to the text field generates an alert
      alert("error! Please input book name");
     return
    };
    const newBooks = books.map(book => {
      return {...book}
    });
    setBooks([...books, {deleteFlag: false, title: newTitle, date: new Date()}]);
    setNewTitle("");
  }
//change line colour
  
  function deleteToggle(i) {
    const newBooks = books.map(book => {
      return {...book}
    });
    newBooks[i].deleteFlag = !newBooks[i].deleteFlag;
    setBooks(newBooks);

  }

  function deleteBooks() {
    let newBooks = books.map(book => {
      return {...book}
    });
    newBooks = newBooks.filter(book => book.deleteFlag === false);
    setBooks(newBooks);
  }
  
 

  
//pressing enter adds a book

function enterKey() {
  console.log("loaded enterKey");
  window.addEventListener("keypress", function(event) {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("bookAddBtn").click();
      }
    });
  }
 
//focus on text box

 function getFocus(){
  console.log("loaded getFocus");
    document.getElementById("myTextField").focus();
    
  }

  //allows multiple functions to be loaded through onload
  function multiLoad(){
    console.log("loaded multiLoad");
    getFocus();
    enterKey();
    }
  
  window.onload = multiLoad;
//----------------------------------
  
  const deleteDisabled = books.reduce((acc, book) => book.deleteFlag === false && acc, true);

  return (
    
    <div className="App" >
      <div class='main'>
     <div class="container">
     <div  class="page-header"> 
      <h1>jQuery To-Do Application</h1>
    </div>
    
      <form  >
        <input id="myTextField" type="text" value={newTitle} onChange={e=>setNewTitle(e.target.value)}  placeholder="Add an item..."></input>
      
      </form>


      <div>
        <button id="bookAddBtn" onClick={addBook}>Add</button>
        <button disabled={deleteDisabled} onClick={deleteBooks}>Delete</button>
      </div>

      </div>
      </div >
      <div id="bookList"  >
      <List  books={books} delete={deleteToggle}   ></List>
      </div >
    </div>
    
  );
}

export default App;
